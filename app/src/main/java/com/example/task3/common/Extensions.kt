package com.example.task3.common

import android.content.Intent
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity

//fun AppCompatActivity.resultActivityLauncher(intent: Intent, result: (ActivityResult) -> Unit) = registerForActivityResult(
//    ActivityResultContracts.StartActivityForResult()
//) { result(it) }.launch(intent)
//
//fun AppCompatActivity.requestPermissionLauncher(permission: String, isGranted: (Boolean) -> Unit) = registerForActivityResult(
//    ActivityResultContracts.RequestPermission()
//) { isGranted(it) }.launch(permission)