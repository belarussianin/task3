package com.example.task3.domain.repository

import com.example.task3.domain.model.Contact
import kotlinx.coroutines.flow.Flow

interface ContactsRepository {
    suspend fun saveContactInRoom(contact: Contact)
    suspend fun getAllContactsFromRoom(): Flow<List<Contact>>
    suspend fun getContactById(id: Int): Contact
    suspend fun getContactIdFromSettings(): Flow<Int?>
    suspend fun saveContactIdToSettings(id: Int)
}