package com.example.task3.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.task3.data.database.COLUMN_NAME_ID
import com.example.task3.data.database.COLUMN_NAME_MAIL
import com.example.task3.data.database.COLUMN_NAME_NAME
import com.example.task3.data.database.COLUMN_NAME_PHONE_NUMBER
import com.example.task3.data.database.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class Contact(
    @PrimaryKey @ColumnInfo(name = COLUMN_NAME_ID) val id: Int,
    @ColumnInfo(name = COLUMN_NAME_PHONE_NUMBER) val number: String?,
    @ColumnInfo(name = COLUMN_NAME_NAME) val name: String?,
    @ColumnInfo(name = COLUMN_NAME_MAIL) val mail: String?
) {
    constructor(id: Int) : this(id, null, null, null)

    override fun toString(): String {
        return "Number: $number\nName: $name\nMail: $mail"
    }
}