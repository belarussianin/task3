package com.example.task3.domain.use_case

import com.example.task3.domain.repository.ContactsRepository

class GetContactIdFromSpUseCase(private val repository: ContactsRepository) {
    suspend fun run() = repository.getContactIdFromSettings()
}