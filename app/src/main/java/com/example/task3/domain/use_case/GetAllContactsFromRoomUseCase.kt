package com.example.task3.domain.use_case

import com.example.task3.domain.model.Contact
import com.example.task3.domain.repository.ContactsRepository
import kotlinx.coroutines.flow.Flow

class GetAllContactsFromRoomUseCase(private val repository: ContactsRepository) {
    suspend fun execute(): Flow<List<Contact>> = repository.getAllContactsFromRoom()
}