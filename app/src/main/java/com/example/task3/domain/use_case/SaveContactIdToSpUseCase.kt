package com.example.task3.domain.use_case

import com.example.task3.domain.repository.ContactsRepository

class SaveContactIdToSpUseCase(private val repository: ContactsRepository) {
    suspend fun run(params: Int) {
        repository.saveContactIdToSettings(params)
    }
}