package com.example.task3.domain.use_case

import com.example.task3.domain.model.Contact
import com.example.task3.domain.repository.ContactsRepository

class SaveContactUseCase(private val repository: ContactsRepository) :
    BaseUseCase<Unit, Contact>() {
    override suspend fun run(params: Contact) {
        repository.saveContactInRoom(params)
    }
}