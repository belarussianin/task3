package com.example.task3.domain.use_case

import com.example.task3.domain.model.Contact
import com.example.task3.domain.repository.ContactsRepository

class GetContactFromRoomUseCase(private val repository: ContactsRepository) :
    BaseUseCase<Contact, Int>() {
    override suspend fun run(params: Int): Contact {
        return repository.getContactById(params)
    }
}