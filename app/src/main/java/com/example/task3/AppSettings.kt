package com.example.task3

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class AppSettings(private val context: Context) {

    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "userSettings")
    private val savedContactID = intPreferencesKey(name = "savedContactID")

    //get the saved setting
    val getSavedContactId: Flow<Int?> = context.dataStore.data.map { preferences ->
        preferences[savedContactID]
    }

    //save setting into datastore
    suspend fun saveContactId(id: Int) {
        context.dataStore.edit { preferences ->
            preferences[savedContactID] = id
        }
    }
}