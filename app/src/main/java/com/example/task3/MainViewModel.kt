package com.example.task3

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.task3.domain.model.Contact
import com.example.task3.domain.use_case.GetAllContactsFromRoomUseCase
import com.example.task3.domain.use_case.GetContactFromRoomUseCase
import com.example.task3.domain.use_case.GetContactIdFromSpUseCase
import com.example.task3.domain.use_case.SaveContactIdToSpUseCase
import com.example.task3.domain.use_case.SaveContactUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class MainViewModel(
    private val saveContactUseCase: SaveContactUseCase,
    private val getAllContactsFromRoomUseCase: GetAllContactsFromRoomUseCase,
    private val saveContactIdToSpUseCase: SaveContactIdToSpUseCase,
    private val getContactIdFromSpUseCase: GetContactIdFromSpUseCase,
    private val getContactFromRoomUseCase: GetContactFromRoomUseCase
) : ViewModel() {
    private val _contactFromSp = MutableStateFlow<Contact?>(null)
    val contactFromSp: StateFlow<Contact?> get() = _contactFromSp

    private val _contactsFromRoom = MutableStateFlow<List<Contact>>(emptyList())
    val contactsFromRoom: StateFlow<List<Contact>> get() = _contactsFromRoom

    init {
        viewModelScope.launch(Dispatchers.IO) {
            getContactIdFromSpUseCase.run()
                .onEach {
                    it?.let {
                        _contactFromSp.value = getContactFromRoomUseCase.run(it)
                    }
                }.launchIn(this)

            getAllContactsFromRoomUseCase.execute()
                .onEach {
                    //Log.i("TAG", it.toString())
                    _contactsFromRoom.value = it
                }.launchIn(this)
        }
    }

    fun saveContact(id: Int, number: String?, name: String?, email: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            saveContactUseCase.run(Contact(id, number, name, email))
        }
    }

    fun saveContactIdToSettings(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            saveContactIdToSpUseCase.run(id)
        }
    }
}