package com.example.task3.di

import com.example.task3.data.repository.ContactsRepositoryImpl
import com.example.task3.domain.use_case.GetAllContactsFromRoomUseCase
import com.example.task3.domain.use_case.GetContactFromRoomUseCase
import com.example.task3.domain.use_case.GetContactIdFromSpUseCase
import com.example.task3.domain.use_case.SaveContactIdToSpUseCase
import com.example.task3.domain.use_case.SaveContactUseCase
import org.koin.dsl.module

val usecaseModule = module {
    factory { SaveContactUseCase(get<ContactsRepositoryImpl>()) }
    factory { GetContactFromRoomUseCase(get<ContactsRepositoryImpl>()) }
    factory { GetAllContactsFromRoomUseCase(get<ContactsRepositoryImpl>()) }

    factory { SaveContactIdToSpUseCase(get<ContactsRepositoryImpl>()) }
    factory { GetContactIdFromSpUseCase(get<ContactsRepositoryImpl>()) }
}