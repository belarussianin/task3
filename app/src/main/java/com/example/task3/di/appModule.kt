package com.example.task3.di

import com.example.task3.AppSettings
import com.example.task3.data.database.ContactsDatabase
import com.example.task3.data.repository.ContactsRepositoryImpl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import org.koin.dsl.module

val appModule = module {
    single { CoroutineScope(SupervisorJob()) }
    // App Settings
    single { AppSettings(get()) }
    //Contacts Room DB
    single { ContactsDatabase.getDatabase(get(), get()) }
    //Repository
    single { ContactsRepositoryImpl(get<ContactsDatabase>().contactsDao(), get()) }
}