package com.example.task3.data.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.task3.domain.model.Contact
import kotlinx.coroutines.flow.Flow

@Dao
interface ContactsDao {

    @Query("SELECT * FROM $TABLE_NAME")
    fun getAll(): Flow<List<Contact>>

    @Query("SELECT * FROM $TABLE_NAME WHERE $COLUMN_NAME_ID = :id")
    fun getById(id: Int): Contact

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(contact: Contact)

    @Delete
    suspend fun delete(contact: Contact)
}