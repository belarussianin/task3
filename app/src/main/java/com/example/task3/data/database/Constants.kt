package com.example.task3.data.database

//Database info
const val DATABASE_NAME = "contacts_db"
const val DATABASE_VERSION = 2

//Table info
const val TABLE_NAME = "contacts_table"
const val COLUMN_NAME_ID = "contacts_column_id"
const val COLUMN_NAME_PHONE_NUMBER = "contacts_column_number"
const val COLUMN_NAME_NAME = "contacts_column_name"
const val COLUMN_NAME_MAIL = "contacts_column_mail"