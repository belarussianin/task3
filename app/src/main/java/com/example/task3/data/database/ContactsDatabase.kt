package com.example.task3.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.task3.domain.model.Contact
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = [Contact::class], version = DATABASE_VERSION, exportSchema = false)
abstract class ContactsDatabase : RoomDatabase() {

    abstract fun contactsDao(): ContactsDao

    companion object {

        @Volatile
        private var INSTANCE: ContactsDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): ContactsDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ContactsDatabase::class.java,
                    DATABASE_NAME
                )
                    .addCallback(ContactsDatabaseCallback(scope))
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }

        private class ContactsDatabaseCallback(
            private val scope: CoroutineScope
        ) : RoomDatabase.Callback() {

            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                // If you want to keep the data through app restarts,
                // comment out the following line.
                INSTANCE?.let { database ->
                    scope.launch {
                        populateDatabase(database.contactsDao())
                    }
                }
            }

            suspend fun populateDatabase(contactsDao: ContactsDao) {
                getSampleList().forEach {
                    contactsDao.insert(it)
                }
            }
        }

        fun getSampleList() = listOf<Contact>(
//            Contact(
//                id = 0,
//                number = "example",
//                null,
//                null
//            ),
//            Contact(
//                id = 1,
//                number = "+37529228336"
//            ),
//            Contact(
//                phoneNumber = "kek",
//                firstName = "lol",
//                lastName = null,
//                mail = null
//            )
        )
    }
}