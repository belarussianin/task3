package com.example.task3.data.repository

import com.example.task3.AppSettings
import com.example.task3.data.database.ContactsDao
import com.example.task3.domain.model.Contact
import com.example.task3.domain.repository.ContactsRepository

class ContactsRepositoryImpl(
    private val contactsDao: ContactsDao,
    private val appSettings: AppSettings
) : ContactsRepository {

    override suspend fun saveContactInRoom(contact: Contact) = contactsDao.insert(contact)

    override suspend fun getAllContactsFromRoom() = contactsDao.getAll()

    override suspend fun getContactById(id: Int): Contact = contactsDao.getById(id)

    override suspend fun getContactIdFromSettings() = appSettings.getSavedContactId

    override suspend fun saveContactIdToSettings(id: Int) = appSettings.saveContactId(id)
}