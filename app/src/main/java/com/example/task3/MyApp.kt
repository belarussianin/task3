package com.example.task3

import android.app.Application
import com.example.task3.di.appModule
import com.example.task3.di.usecaseModule
import com.example.task3.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin {
            //androidLogger(Level.DEBUG)
            androidContext(this@MyApp)
            modules(listOf(appModule, usecaseModule, viewModelModule))
        }
    }
}