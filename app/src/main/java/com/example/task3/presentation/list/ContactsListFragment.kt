package com.example.task3.presentation.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.example.task3.MainViewModel
import com.example.task3.databinding.FragmentListContactsBinding
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.getViewModel

class ContactsListFragment : Fragment() {

    private var _binding: FragmentListContactsBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentListContactsBinding.inflate(inflater, container, false).also {
        _binding = it
        viewModel = getViewModel()
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.list.adapter = ContactsListAdapter {
            val snackBar = Snackbar.make(binding.list, it.toString(), Snackbar.LENGTH_LONG)
                .setAction("CANCEL") {}
            snackBar.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text).maxLines = 10
            snackBar.show()
            viewModel.saveContactIdToSettings(it.id)
            findNavController().popBackStack()
        }.apply {
            viewLifecycleOwner.lifecycleScope.launch {
                viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.contactsFromRoom.collectLatest {
                        submitList(it)
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}