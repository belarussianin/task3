package com.example.task3.presentation.main

import android.Manifest
import android.app.Activity
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.DrawableRes
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.task3.MainViewModel
import com.example.task3.R
import com.example.task3.databinding.FragmentMainBinding
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.getViewModel


class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: MainViewModel

    private val pickContactLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { it.pickContactResultParse() }

    private val requestReadContactPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            pickContactLauncher.launch(
                Intent(
                    Intent.ACTION_PICK,
                    ContactsContract.Contacts.CONTENT_URI
                )
            )
        } else {
            Toast.makeText(
                requireContext(),
                "Action not possible without permission",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentMainBinding.inflate(inflater, container, false).also {
        _binding = it
        viewModel = getViewModel()
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.bindUI()
    }

    private fun FragmentMainBinding.bindUI() {
        contactPick.setOnClickListener {
            when (
                ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.READ_CONTACTS
                )
            ) {
                PackageManager.PERMISSION_GRANTED -> {
                    pickContactLauncher.launch(
                        Intent(
                            Intent.ACTION_PICK,
                            ContactsContract.Contacts.CONTENT_URI
                        )
                    )
                }
                else -> {
                    requestReadContactPermissionLauncher.launch(Manifest.permission.READ_CONTACTS)
                }
            }
        }
        contactsShow.setOnClickListener {
            findNavController().navigate(R.id.contactsListFragment)
        }
        contactPickFromPrefs.setOnClickListener {
            val snackBar = Snackbar.make(
                binding.root,
                viewModel.contactFromSp.value.toString(),
                Snackbar.LENGTH_LONG
            )
                .setAction("CANCEL") {}
            snackBar.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text).maxLines =
                10
            snackBar.show()
        }
        contactShowNotification.setOnClickListener {
            viewModel.contactFromSp.value?.let { contact ->
                val manager = getSystemService(requireContext(), NotificationManager::class.java)
                if (Build.VERSION.SDK_INT >= 26) {
                    //When sdk version is larger than26
                    val id = "channel_1"
                    val description = "228"
                    val importance = NotificationManager.IMPORTANCE_LOW
                    val channel = NotificationChannel(id, description, importance)

                    manager?.createNotificationChannel(channel)
                    val notification: Notification = Notification.Builder(requireContext(), id)
                        .setSmallIcon(R.drawable.ic_baseline_people_24)
                        .setContentTitle("Number: ${contact.number}")
                        .setContentText("Name: ${contact.name} Mail: ${contact.mail}")
                        .setAutoCancel(true)
                        .build()
                    manager?.notify(1, notification)
                } else {
                    //When sdk version is less than26
                    val notification: Notification = NotificationCompat.Builder(requireContext())
                        .setSmallIcon(R.drawable.ic_baseline_people_24)
                        .setContentTitle("Number: ${contact.number}")
                        .setContentText("Name: ${contact.name} Mail: ${contact.mail}")
                        .setAutoCancel(true)
                        .build()
                    manager?.notify(1, notification)
                }
            }
            viewModel.contactFromSp.value ?: Toast.makeText(
                requireContext(),
                "Nothing to show",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun ActivityResult.pickContactResultParse() {
        if (resultCode == Activity.RESULT_OK) {
            val contactData: Uri? = data?.data
            contactData?.let {
                activity?.contentResolver?.query(it, null, null, null, null)
            }?.let { cursor ->
                with(cursor) {
                    var id: Int? = null
                    var number: String? = null
                    var name: String? = null
                    var email: String? = null

                    if (moveToFirst()) {
                        val contactIndex =
                            getColumnIndex(ContactsContract.Contacts._ID)
                        if (contactIndex != -1) {
                            val contactId = getString(contactIndex)
                            with(
                                activity?.contentResolver?.query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId,
                                    null,
                                    null
                                )
                            ) {
                                this?.let {
                                    if (moveToNext()) {
                                        var index =
                                            getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                                        number = getString(index)

                                        index =
                                            getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY)
                                        name = getString(index)

                                        index =
                                            getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID)
                                        id = getInt(index)
                                    }
                                }
                            }

                            with(
                                activity?.contentResolver?.query(
                                    ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId,
                                    null,
                                    null
                                )
                            ) {
                                this?.let {
                                    if (moveToNext()) {
                                        val index =
                                            getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS)
                                        email = getString(index)
                                    }
                                }
                            }
                        }
                    }
                    if (id != null) {
                        Toast.makeText(
                            requireContext(),
                            "number: $number\nname: $name\nemail: $email\nid: $id",
                            Toast.LENGTH_SHORT
                        ).show()

                        id?.let { id ->
                            viewModel.saveContact(
                                id,
                                number,
                                name,
                                email
                            )
                        }
                        Toast.makeText(
                            requireContext(),
                            resources.getString(R.string.success_save),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Toast.makeText(
                            requireContext(),
                            resources.getString(R.string.failure_save),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}